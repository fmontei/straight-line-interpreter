package project;

public interface Formattable {

	String toFormattedString();
	String toString();
}
