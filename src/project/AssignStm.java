package project;

public class AssignStm implements Stm {

	private String m_Id;
	private Exp m_rhs;
	// The singleton SymbolTable is instantiated in AssignStm() because the
	// SymbolTable is responsible for retaining the mapped assignments carried
	// out by AssignStm's execute() method
	private static SymbolTable m_symbolTable = SymbolTable.getInstance();
	
	public AssignStm(String Id, Exp rhs) {
		
		m_Id = Id;
		m_rhs = rhs;
	}

	// AssignStm is clearly responsible for assigning numerical values to
	// string expressions; hence, execute() performs this task. 
	public void execute() {
		
		m_symbolTable.assign(m_Id, m_rhs.evaluate());
	}
	
	// Produces part of the output specified in Listing 2 in the rubric
	public String toFormattedString() {
		
		return (m_Id + " := " + m_rhs.toFormattedString() + ";"); 
	}
	
	// Getter method called by IdExp, as specified implicitly by the UML diagram
	public static SymbolTable getSymbolTable() {
		
		return m_symbolTable;
	}
}
