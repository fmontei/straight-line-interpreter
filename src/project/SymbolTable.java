package project;

import java.util.HashMap;

// Singleton responsible for storing String-Integer assignments, where the
// Integer value is assigned to the String 
public class SymbolTable implements Formattable {
	
	private static SymbolTable instance = null;
	HashMap <String, Integer> m_table = new HashMap <String, Integer> ();
	
	private SymbolTable() {
		
	}
	
	public int lookup(String Id) {
		
		Integer result = m_table.get(Id);
		
		if (result != null)
			return result;
		
		return 0;
	}
	
	public void assign(String Id, int value) {
		
		m_table.put(Id, value);
	}

	protected static SymbolTable getInstance() {
		
		if (instance == null) {
			
			instance = new SymbolTable();
		}
		
		return instance;
	}

	// toFormattedString() implementation is unnecessary here
	public String toFormattedString() {
		
		return "";
	}
}
