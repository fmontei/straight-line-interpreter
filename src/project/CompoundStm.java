package project;

// CompoundStm is responsible for "extending" the nested constructors found in
// MainDriver
public class CompoundStm implements Stm {

	Stm m_stm1;
	Stm m_stm2;
	
	public CompoundStm(Stm stm1, Stm stm2) {
		
		m_stm1 = stm1;
		m_stm2 = stm2;
	}
	
	// Calls the execute method of the two Stms passed to CompoundStm
	public void execute() {
		
		m_stm1.execute();
		m_stm2.execute();
	}
	
	public String toFormattedString() {
		
		return (m_stm1.toFormattedString() + "\n" + m_stm2.toFormattedString());
	}
}
