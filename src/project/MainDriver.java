/*
 * Name:       Felipe Monteiro
 * Comment:    Program that employs the interpreter pattern. The test case
 * 			   in MainDriver uses AssignStm()'s of different lengths in order
 *             to verify that the class is working as intended. toString() 
 *             has not been implemented because of its lack of necessity.
 *             
 *             PrintStm() is responsible for displaying the numerical output
 *             of the program. toFormattedString() is responsible for 
 *             creating a syntactical layout similar to Listing 2 in the 
 *             assignment rubric. 
 */

package project;

public class MainDriver {
	
	public static void main(String[] args) {
			
		Stm stm = new CompoundStm(
			new AssignStm("a", new NumExp(2)), new CompoundStm(
			new AssignStm("b", new NumExp(3)), new CompoundStm(
			new AssignStm("d",
			new OpExp(new IdExp("a"), Op.TIMES, new IdExp("b"))),
			new CompoundStm(
			new AssignStm("e", 
			new OpExp(
			new OpExp(new IdExp("d"), Op.PLUS, new IdExp("d")),
			Op.MINUS,
			new OpExp(new IdExp("d"), Op.DIV, new NumExp(2)))),	
			new CompoundStm(
			new AssignStm("f", 
			new OpExp(
			new OpExp(new IdExp("e"), Op.PLUS, new IdExp("d")),
			Op.TIMES,
			new OpExp(new IdExp("e"), Op.TIMES, new NumExp(4)))),
			new CompoundStm(
			new AssignStm("g", 
			new OpExp(
			new OpExp(new IdExp("f"), Op.PLUS, new IdExp("a")),
			Op.DIV,
			new OpExp(new IdExp("c"), Op.MINUS, new NumExp(5)))),
			new CompoundStm(
			new AssignStm("h", 
			new OpExp(new OpExp(
			new OpExp(new IdExp("g"), Op.PLUS, new IdExp("a")),
			Op.DIV,
			new OpExp(new IdExp("c"), Op.MINUS, new NumExp(5))),
			Op.PLUS, 
			new OpExp(new IdExp("f"), Op.PLUS, new IdExp("a")))),
			new CompoundStm(
			new AssignStm("i", 
			new OpExp(new OpExp(
			new OpExp(new IdExp("g"), Op.PLUS, new IdExp("e")),
			Op.TIMES,
			new OpExp(new IdExp("c"), Op.MINUS, new NumExp(2))),
			Op.PLUS, 
			new OpExp(
			new OpExp(new IdExp("g"), Op.PLUS, new IdExp("f")),
			Op.MINUS,
			new OpExp(new IdExp("d"), Op.MINUS, new NumExp(6))))),
			new CompoundStm(
			new PrintStm(new IdExp("a")), new CompoundStm(
			new PrintStm(new IdExp("b")), new CompoundStm(
			new PrintStm(new IdExp("c")), new CompoundStm(
			new PrintStm(new IdExp("d")), new CompoundStm(
			new PrintStm(new IdExp("e")), new CompoundStm(
			new PrintStm(new IdExp("f")), new CompoundStm(
			new PrintStm(new IdExp("g")), new CompoundStm(
			new PrintStm(new IdExp("h")),
			new PrintStm(new IdExp("i"))))))))))))))))));
			
		stm.execute();
		
		System.out.println();
		System.out.println(stm.toFormattedString());
	}
}
