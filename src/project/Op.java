package project;

public enum Op {

	PLUS("+"), MINUS("-"), TIMES("*"), DIV("/");
	private String operator;
	
	Op(String operator) {
		
		this.operator = operator;
	}
}
