package project;

public class IdExp implements Exp {

	String m_Id;
	
	public IdExp(String Id) {
		
		m_Id = Id;
	}
	
	// IdExp's evaluate() method looks up the string in the SymbolTable and
	// returns the integer value it was assigned
	public int evaluate() {
		
		return (AssignStm.getSymbolTable().lookup(m_Id));
	}

	public String toFormattedString() {
		
		return (m_Id);
	}
}
