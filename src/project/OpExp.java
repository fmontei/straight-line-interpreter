package project;

public class OpExp implements Exp {

	private Exp m_exp1;
	private Exp m_exp2;
	private Op m_op;

	public OpExp(Exp exp1, Op op, Exp exp2) {
		
		m_exp1 = exp1;
		m_exp2 = exp2;
		m_op = op;		
	}

	public int evaluate() {
		
		switch(m_op) {
		
			case PLUS:
				return (m_exp1.evaluate() + m_exp2.evaluate());
			case MINUS:
				return (m_exp1.evaluate() - m_exp2.evaluate());
			case TIMES:
				return (m_exp1.evaluate() * m_exp2.evaluate());
			case DIV:
				try { // Prevents division by zero from crashing program
					return (m_exp1.evaluate() / m_exp2.evaluate());
				} catch(ArithmeticException e) {
					e.printStackTrace();
				}
			default:
				return 0; // If a variable was never assigned a value, return 0
		}
	}

	public String op() {
		
		String result = "";
		switch(m_op) {
		
			case PLUS:
				result = " + ";
				break;
			case MINUS:
				result = " - ";
				break;
			case TIMES:
				result = " * ";
				break;
			case DIV:
				result = " / ";
				break;
		}
		
		return result;
	}	
	
	public String toFormattedString() {

		return m_exp1.toFormattedString() + op() + m_exp2.toFormattedString();
	}
}
