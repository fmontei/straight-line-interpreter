package project;

public class NumExp implements Exp {

	int m_num;
	
	public NumExp(int num) {
		
		m_num = num;
	}
	
	public int evaluate() {
		
		return m_num;
	}

	// Converts the integer to String format
	public String toFormattedString() {
		
		return (Integer.toString(m_num));
	}
}
