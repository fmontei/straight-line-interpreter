package project;

public class PrintStm implements Stm {

	Exp m_arg;
	
	public PrintStm(Exp arg) {
		
		m_arg = arg;
	}
	
	// Produces part of the output specified in Listing 2 in the rubric
	public String toFormattedString() {
		
		return ("print(" + m_arg.toFormattedString() + ");");
	}

	// PrintStm is responsible for printing the program's output to the 
	// terminal; hence, execute() calls System.out.print() to accomplish this
	public void execute() {
		
		System.out.print(Integer.toString(m_arg.evaluate()));
	}
}
